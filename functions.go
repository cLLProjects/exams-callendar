package main
import ( "time" 
		 "strings"
		 "strconv")

func AgeCalc(birthdate string)string{
	t := time.Now()
	year := t.Year()

	date := strings.Split(birthdate,"/")
	y, _ := strconv.Atoi(date[2])
	age := strconv.Itoa(year - y)
	return age
}