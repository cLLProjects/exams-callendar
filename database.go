package main

import(
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"fmt"
)

var Db *Database

type Database struct{
	instance *sql.DB
	statements map[string]map[string]*sql.Stmt
	action map[string]func(*sql.Stmt, interface{}) (user interface{},err error)
}

var (
	Statements map[string]map[string]*sql.Stmt
)

func startDatabase (){
	
	Db = newDB("userweb","123456",Statements)
	
	Db.statements = loadStatements()

	Db.action = loadActions()
}

func newDB(user string,pass string, statements map[string]map[string]*sql.Stmt) (db *Database) {
	instance, err := sql.Open("mysql", user +":"+ pass +"@tcp(192.168.15.5:3306)/banco")
	db = &Database{
		instance : instance,
		statements: statements,
	} 
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	return db
}

func (db *Database) register(stmtIns *sql.Stmt, user interface{}) (userF interface{},err error) {
	email := user.(User).Email
	pass := user.(User).Pass
	birthdate := user.(User).Birthdate
	userF = user

	_, err = stmtIns.Exec(email, pass, birthdate)
	if err != nil {
		fmt.Printf(err.Error()) // proper error handling instead of panic in your app
	}
	return userF,err
}

func (db *Database) login(stmtIns *sql.Stmt, login interface{}) (user interface{},err error){
	email := login.(LoginJson).Email
	pass := login.(LoginJson).Pass
	var id int
	var email2, password, birthdate string
	err = stmtIns.QueryRow(email,pass).Scan(&id, &email2, &password, &birthdate)
	user = User{Email: email2, Pass: password, Birthdate : birthdate}
	if err != nil {
		return user,err
	}
	return user, err
}
func (db *Database) exists(stmtIns *sql.Stmt, user interface{}) (login interface{}, err error){
	email := user.(User).Email
	var id int
	var email2, password, birthdate string
	err = stmtIns.QueryRow(email).Scan(&id, &email2, &password, &birthdate)
	login = LoginJson{Email: email2, Pass: password}
	if err != nil {
		return login,err
	}
	return login, err
}