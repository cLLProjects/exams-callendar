package main

import(
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"fmt"
)

func getPrepareStatementExists() (stmtIns *sql.Stmt) {
	stmtIns, err := Db.instance.Prepare("SELECT * FROM usuario WHERE email = ?") // ? = placeholder
	if err != nil {
		fmt.Printf(err.Error()) // proper error handling instead of panic in your app
	}
	return stmtIns
}

func getPrepareStatementRegister() (stmtIns *sql.Stmt) {
	stmtIns, err := Db.instance.Prepare("INSERT INTO usuario (email,senha,birthdate) VALUES( ?, ?, ?)") // ? = placeholder
	if err != nil {
		fmt.Printf(err.Error()) // proper error handling instead of panic in your app
	}
	return stmtIns
}

func getPrepareStatementLogin() (stmtIns *sql.Stmt){
	stmtIns, err := Db.instance.Prepare("SELECT * FROM usuario WHERE email = ? AND senha = ?")
	if err != nil {
		fmt.Printf(err.Error())
	}
	return stmtIns
}
func loadActions() (action map[string]func(*sql.Stmt, interface{}) (user interface{},err error)){
	action = map[string]func(*sql.Stmt, interface{}) (user interface{},err error){
		"login": Db.login,
		"register": Db.register,
		"exists" : Db.exists,
	}
	return action
}
func loadStatements() (statements map[string]map[string]*sql.Stmt){
	statements = map[string]map[string]*sql.Stmt{
		"form": startStatementsForm(),
	}
	return statements
}
func startStatementsForm() (formStatementsForm map[string]*sql.Stmt){
	formStatementsForm = map[string]*sql.Stmt{
		"login": getPrepareStatementLogin(),
		"register": getPrepareStatementRegister(),
		"exists" : getPrepareStatementExists(),
	}
	return formStatementsForm
}