package main
import ( "net/http"
		 "log"
		 "io/ioutil"
		 "encoding/json")

func Login(res http.ResponseWriter, req *http.Request){
	var u LoginJson
	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)
	errS := val.Struct(u)

	if errS != nil{
		log.Println("[INFO] A user failed on login, validation error:")
		log.Print(errS)
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte("{message: invalid body}"))
		return
	}
	user,err := u.login()
	if err != nil{
		res.WriteHeader(http.StatusForbidden)
		return
	}
	res.WriteHeader(http.StatusOK)
	res.Write([]byte("{message: user successfully login, your age is " +""+ AgeCalc(user.(User).Birthdate) + "}"))
	log.Println("[INFO] A user logged in successfully.")

}