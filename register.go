package main
import ( "net/http"
		 "log"
		 "io/ioutil"
		 "encoding/json")

func Register(res http.ResponseWriter, req *http.Request) {
	if req.Method != "POST"{
		res.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	if req.Header.Get("Content-Type") != "application/json"{
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte("{message: invalid header}"))
		return
	}
	var u User
	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)
	errV := val.Struct(u)

	if errV != nil{
		log.Println("[INFO] A user failed on register, validation error:")
		log.Print(errV)
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte("{message: invalid body}"))
		return
	}
	_,err := u.exist()
	if err != nil {
		u.register()
		res.WriteHeader(http.StatusOK)
		res.Write([]byte("{message: user successfully registered}"))
		log.Println("[INFO] A user has been cadastred.")
		return
	}
	res.WriteHeader(http.StatusForbidden)
	res.Write([]byte("{message: this email already exist}"))
	log.Println("[INFO] User register error")
}