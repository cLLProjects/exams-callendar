package main

import ( "net/http"
		 "time" 
		 "log"
		 "github.com/gorilla/mux"
		 "gopkg.in/go-playground/validator.v9"
		_ "github.com/go-sql-driver/mysql")

var val = validator.New()

func main() {

	startDatabase()

	StartServer()

	defer Db.instance.Close()

	log.Println("[INFO] Server shutting down.")
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/register",Register)
	r.HandleFunc("/login",Login)

	server := &http.Server{
			Addr       : "192.168.15.5:89",
			IdleTimeout: duration,
			Handler    : r, 
	}
	log.Println("[INFO] Server HTTP Online! Adress ---->"+ server.Addr + ".")
	log.Print(server.ListenAndServe())
}

