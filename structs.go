package main

type User struct {
	Email   string `json:"email" validate:"required,email"` 
	Pass  string `json:"pass" validate:"required"`
	Birthdate   string `json:"birthdate" validate:"required"` 
}

type LoginJson struct{
	Email   string `json:"email" validate:"required,email"` 
	Pass  string `json:"pass" validate:"required"`
}

func (u User) register()(user interface{},err error){
	return Db.register(Db.statements["form"]["register"],u)
}
func (u User) exist()(user interface{},err error){
	return Db.exists(Db.statements["form"]["exists"],u)
}
func (l LoginJson) login() (user interface{},err error){
	return Db.login(Db.statements["form"]["login"],l)
}